import requests
import json
import pickle
import maya
from datetime import date
from pprint import pprint
from pyzillow.pyzillow import ZillowWrapper, GetDeepSearchResults

ATT_URL = 'https://www.att.com/services/shop/model/ecom/shop/view/unified/' \
'qualification/service/CheckAvailabilityRESTService/'\
'invokeCheckAvailability?_=1486144182795&_dynSessConf='\
'-868602334025573224'

DEFAULT_DICT = {'maxHsiaSpeedAvailable': 0}


def check_att_service(address, zipcode):
    '''returns att service availability object as json'''

    result = requests.post(
        ATT_URL, data={
            'userInputZip': zipcode,
            'userInputAddressLine1': address,
            'mode': 'fullAddress'
            }
    )
    return result


def add_new_house(houses):
    '''convenience method to add new homes'''
    required_inputs = ['street', 'city', 'state', 'zipcode']
    optional_inputs = ['rent', 'realty_agent', 'listing_date']

    house_dict = {}
    for entry in required_inputs:
        house_dict[entry] = input('{}: '.format(entry.title().replace('_', ' ')))

    for entry in optional_inputs:
        data = input('{}: '.format(entry.title()))
        if data == 'x':
            break
        else:
            house_dict[entry] = data
    new_house = PotentialAddress(**house_dict)
    houses.append(new_house)


def get_max_speed(data):
    '''returns max speed available'''

    if 'json' in dir(data):
        data = data.json()

    if 'CkavDataBean' in data:
        return data.get('CkavDataBean', DEFAULT_DICT).get('maxHsiaSpeedAvailable', 0)


def get_var_name(variable):
    '''returns variable name'''

    return [k for k, v in globals().items() if v is variable][0]

def generate_csv_file(houses, filename):
    with open(filename, 'w') as file:
        file.write('Name,Address,Rent,Realty Agent,Speed,'\
        'Beds,Baths,Square Feet,URL,Notes\r\n')
        for house in houses:
            file.write('{}\r\n'.format(house.csv_format()))

def save_json(data):
    '''saves object to json formatted file'''

    file_name = '{}.json'.format(get_var_name(data))
    with open(file_name, 'w') as file:
        json.dump(data, file)


def save_pickle(data):
    ''' saves object to pickle file'''

    file_name = '{}.p'.format(get_var_name(data))
    with open(file_name, 'wb') as file:
        pickle.dump(data, file)


class PotentialAddress(object):
    '''potential address for house hunting'''

    _zillow_api = ZillowWrapper('X1-ZWz1fp4y6vlukr_8l13j')

    def __init__(self, street, city, state, zipcode,
                 rent=-1, realty_agent='', listing_date='today',
                 beds=None, baths=None, square_feet=None, url=None):
        self.street = street
        self.city = city
        self.state = state
        self.zipcode = zipcode
        self.speed = get_max_speed(check_att_service(street, zipcode))
        self.rent = rent
        self.realty_agent = realty_agent
        self.listing_date = maya.when(listing_date)
        self.zillow_data = self.get_zillow_results()
        self.beds = beds or self.zillow_data.bedrooms
        self.baths = baths or self.zillow_data.bathrooms
        self.square_feet = square_feet or self.zillow_data.home_size
        self.url = url or self.zillow_data.home_detail_link
        self.zid = self.zillow_data.zillow_id
        self.notes = []

    def add_note(self, note):
        '''add note'''
        self.notes.append(note)

    def __str__(self):
        return '{}\n{}, {} {}\nInternet: {}\nNotes: {}'.format(
            self.street,
            self.city, self.state, self.zipcode,
            self.speed,
            '\n'.join(self.notes)
        )

    def __repr__(self):
        return 'PotentialAddress({}, {}, {}, {})'.format(
            self.street, self.city, self.state, self.zipcode
        )

    # @property
    # def notes(self):
    #     return '\n'.join(self._notes)

    def all_data(self):
        return '\n'.join(['{}: {}'.format(k, v) for k, v in self.__dict__.items()])

    def get_zillow_results(self):
        return GetDeepSearchResults(PotentialAddress._zillow_api.get_deep_search_results(self.street, self.zipcode))

    def csv_format(self):
        return '{0},{street} {city} {zipcode},${rent:.2f},{realty_agent},{speed},{beds},{baths},{square_feet},{url},{1}'.format(self.street.split(' ', maxsplit=1)[1:][0], ' | '.join(self.notes), **self.__dict__)